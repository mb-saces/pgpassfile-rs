use std::collections::BTreeMap;
use std::fs::File;
use std::io::{BufRead, BufReader, Error, ErrorKind};
use std::path::Path;

const SEPARATOR: char = ':';
const ESCAPE: char = '\\';

fn tokenize(string: &str) -> Vec<String> {
    let mut token = String::new();
    let mut tokens: Vec<String> = Vec::new();
    let mut chars = string.chars();
    while let Some(ch) = chars.next() {
        match ch {
            SEPARATOR => {
                tokens.push(token);
                token = String::new();
            }
            ESCAPE => {
                if let Some(next) = chars.next() {
                    token.push(next);
                }
            }
            _ => token.push(ch),
        }
    }
    tokens.push(token);
    tokens
}

fn pg_pass_file(
    passfile: &String,
    hostname: &String,
    port: &String,
    database: &String,
    username: &String,
) -> Result<String, Error> {
    let pg_pass_file = File::open(Path::new(passfile))?;

    let reader = BufReader::new(pg_pass_file);

    for line in reader.lines() {
        let params = tokenize(line.as_ref().unwrap());

        if params.len() < 5 {
            return Err(Error::new(
                ErrorKind::Other,
                "pgpass line too short",
            ));
        }

        if (hostname == "*" || hostname == &params[0] || &params[0] == "*")
            && (port == "*" || port == &params[1] || &params[1] == "*")
            && (database == "*" || database == &params[2] || &params[2] == "*")
            && (username == "*" || username == &params[3] || &params[3] == "*")
        {
            return Ok(params[4].to_string());
        }
    }
    //return an empty string if no match
    return Ok("".to_string());
}

pub fn passfilehack(db_url: &String) -> Result<String, Error> {
    let mut params = db_url
        .split(' ')
        .map(|kv| kv.split('='))
        .map(|mut kv| (kv.next().unwrap().into(), kv.next().unwrap().into()))
        .collect::<BTreeMap<String, String>>();

    let passfile = params.get("passfile").unwrap();

    let pgpass = pg_pass_file(
        passfile,
        params.get("host").unwrap_or(&"*".to_string()),
        params.get("port").unwrap_or(&"*".to_string()),
        params.get("dbname").unwrap_or(&"*".to_string()),
        params.get("user").unwrap_or(&"*".to_string()),
    );

    if pgpass.is_err() {
        return Err(pgpass.unwrap_err());
    }

    params.remove("passfile");

    params.insert("password".to_string(), pgpass.unwrap());

    let res = params
        .iter()
        .map(|(k, v)| format!("{}={}", k, v))
        .collect::<Vec<String>>()
        .join(" ");
    return Ok(res);
}
