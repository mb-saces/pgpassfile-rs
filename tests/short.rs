use pgpassfile;

#[test]
#[should_panic(expected = "pgpass line too short")]
fn connectstring_single_short() {
    pgpassfile::passfilehack(&"host=host port=port dbname=dbname user=user passfile=./tests/single-short.pgpass".to_string()).unwrap();
}

#[test]
#[should_panic(expected = "pgpass line too short")]
fn connectstring_multi_short() {
    pgpassfile::passfilehack(&"host=host port=port dbname=dbname user=user passfile=./tests/multi-short.pgpass".to_string()).unwrap();
}
