use pgpassfile;


#[test]
fn connectstring_single_passfile_ok() {
    assert_eq!(
        "dbname=dbname host=hostname password=drowssap port=port user=dbuser",
         pgpassfile::passfilehack(&"host=hostname port=port dbname=dbname user=dbuser passfile=./tests/single.pgpass".to_string()).unwrap()
    );
}

#[test]
fn connectstring_single_passfile_no_item() {
    assert_eq!(
        "dbname=dbname host=otherhost password= port=port user=dbuser",
         pgpassfile::passfilehack(&"host=otherhost port=port dbname=dbname user=dbuser passfile=./tests/single.pgpass".to_string()).unwrap()
    );
}



#[test]
fn connectstring_multi_passfile_no_item() {
    assert_eq!(
        "dbname=dbname host=otherhost password= port=port user=dbuser",
         pgpassfile::passfilehack(&"host=otherhost port=port dbname=dbname user=dbuser passfile=./tests/multi.pgpass".to_string()).unwrap()
    );
}

#[test]
fn connectstring_multi_passfile_01() {
    assert_eq!(
        "host=hostname password=drowssap01",
         pgpassfile::passfilehack(&"host=hostname passfile=./tests/multi.pgpass".to_string()).unwrap()
    );
}

#[test]
fn connectstring_multi_passfile_02() {
    assert_eq!(
        "host=hostname password=drowssap02 port=port",
         pgpassfile::passfilehack(&"host=hostname port=port passfile=./tests/multi.pgpass".to_string()).unwrap()
    );
}

#[test]
fn connectstring_multi_passfile_03() {
    assert_eq!(
        "dbname=dbname host=hostname password=drowssap03",
         pgpassfile::passfilehack(&"host=hostname dbname=dbname passfile=./tests/multi.pgpass".to_string()).unwrap()
    );
}

#[test]
fn connectstring_multi_passfile_04() {
    assert_eq!(
        "host=hostname password=drowssap04 user=dbuser",
         pgpassfile::passfilehack(&"host=hostname user=dbuser passfile=./tests/multi.pgpass".to_string()).unwrap()
    );
}
