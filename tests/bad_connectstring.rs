use pgpassfile;

#[test]
#[should_panic(expected = "called `Option::unwrap()` on a `None` value")]
fn bad_connectstring_empty() {
    pgpassfile::passfilehack(&"".to_string()).unwrap();
    //assert_eq!("4", pgpassfile::passfilehack(&"db_url".to_string()).unwrap());
}

#[test]
#[should_panic(expected = "called `Option::unwrap()` on a `None` value")]
fn bad_connectstring_no_space_no_equal() {
    pgpassfile::passfilehack(&"invalidconnectstringwithoutspacesorequalsign".to_string()).unwrap();
}

#[test]
#[should_panic(expected = "called `Option::unwrap()` on a `None` value")]
fn bad_connectstring_no_no_equal() {
    pgpassfile::passfilehack(&"still invalid connect string without equal sign".to_string()).unwrap();
}

#[test]
#[should_panic(expected = "called `Option::unwrap()` on a `None` value")]
fn bad_connectstring_1() {
    pgpassfile::passfilehack(&"still=invalid connect string = sign".to_string()).unwrap();
}

#[test]
#[should_panic(expected = "called `Option::unwrap()` on a `None` value")]
fn bad_connectstring_2() {
    pgpassfile::passfilehack(&"still=invalid connect string=sign".to_string()).unwrap();
}


#[test]
#[should_panic(expected = "called `Option::unwrap()` on a `None` value")]
fn bad_connectstring_bad_spacing_1() {
    pgpassfile::passfilehack(&"still = invalid connect = string foo = bar".to_string()).unwrap();
}

#[test]
#[should_panic(expected = "called `Option::unwrap()` on a `None` value")]
fn bad_connectstring_bad_spacing_2() {
    pgpassfile::passfilehack(&"still= invalid connect=string foo=bar".to_string()).unwrap();
}

#[test]
#[should_panic(expected = "called `Option::unwrap()` on a `None` value")]
fn bad_connectstring_bad_spacing_3() {
    pgpassfile::passfilehack(&"still=invalid connect =string foo=bar".to_string()).unwrap();
}

